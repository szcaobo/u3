/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for face process.
 *
 * Author : zhuangxiaojian
 * Email  : zhuangxj@gionee.com
 * Date   : 2014-10-28
 *
 *************************************************************************************/

#ifndef ANDROID_ARCSOFT_FACE_PROCESS_H
#define ANDROID_ARCSOFT_FACE_PROCESS_H

#include "abstypes.h"
#include "beauty_shot.h"

#include "ammem.h"
#include "merror.h"

#include "GNCameraFeatureListener.h"
#include "GNCameraFeatureDefs.h"

#define BUFFER_SIZE 15*1024

namespace android {
class ArcSoftFaceProcess {
private:
	ArcSoftFaceProcess();
	~ArcSoftFaceProcess();
	

public:
	int32 init();
	void  deinit();
	int32 enableFaceProcess(int featureMask);
	int32 disableFaceProcess(int featureMask);
	int32 faceDetection(LPASVLOFFSCREEN param, PAGE_GENDER_INFO** ageGenderInfo, MLong* faceNum, GNDataType dataType);
	int32 processFaceBeauty(LPASVLOFFSCREEN imgSrc, BEAUTY_PARAM* param, MLong index, GNDataType dataType);
	bool  isFaceProcessEnable() {return mInitialized;};

public:
	static ArcSoftFaceProcess* getInstance();
	void ReadFile(MByte* buffer, int* readSize);

private:	
	static ArcSoftFaceProcess* mArcSoftFaceProcess;
	bool mInitialized;
	int mGNFaceProcessFeature;
	CBeautyShot* mPreviewBeautyShot;
	CBeautyShot* mVideoBeautyShot;
	CBeautyShot* mImageBeautyShot;
	MByte mBuffer[BUFFER_SIZE];
	int mSize;
};
};
#endif/*ANDROID_ARCSOFT_FACE_PROCESS_H*/

