/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for scene detection.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/
 
#ifndef ANDROID_ARCSOFT_SCENE_DETECTION_H
#define ANDROID_ARCSOFT_SCENE_DETECTION_H

#include "ammem.h"
#include "arcsoft_asd.h"
#include "merror.h"
#include "GNCameraFeatureDefs.h"
#include "GNCameraFeatureListener.h"
#include <pthread.h>


namespace android { 
class ArcSoftSceneDetection {
public:
    ArcSoftSceneDetection();
	~ArcSoftSceneDetection();
	
	int32 init();
    void  deinit();
	int32 setCameraListener(GNCameraFeatureListener* listener);
	int32 setOrientation(int orientation);
	int32 processSceneDetection(LPASVLOFFSCREEN param);
	int32 enableSceneDetection();
	int32 disableSceneDetection();

private:
	int32 getSceneDetetionType(int type);
private:
	MByte* mMem;
	MHandle mMemMgr;
	ASD_ENGINE mASD;
	ASD_HW_PARAM mHWParam;

	pthread_mutex_t mMutex;
	bool mInitialized;
	int mOrientation;

	GNCameraFeatureListener* mListener;
	
};
};
#endif /* ANDROID_ARCSOFT_SCENE_DETECTION_H */
