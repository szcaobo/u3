/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/

#include "FrameProcessor.h"

#define DETECTION_MODE    	0    /*0:accurate; 1:fast*/
#define OSDMODE           	0    /*display info*/

FrameProcessor::FrameProcessor()
	: mDefogListener(NULL)
	, mDefogProcessor(NULL)
	, mMemInitParm(NULL)
	, mInitialized(false)
	, mGNFrameProcessMask(0)
{
    if (mDefogProcessor == NULL) {
        mDefogProcessor = new DefogProcessor();
	    if (mDefogProcessor == NULL) {
    		PRINTE("Faile to new DefogProcessor");
	    }
	}

	pthread_mutex_init(&mDefogProcessorMutex, NULL);
	pthread_mutex_init(&mThreadMutex, NULL);
	pthread_cond_init(&mCond, NULL);

	AlgDefogRunTimeParm* runtimeparm = mDefogProcessor->GetDefaltRunTimeparm(OSDMODE);
	mDefogProcessor->Ctrl(runtimeparm);//set tuning param

	memset(&mFogFlareDegree, 0, sizeof(mFogFlareDegree));		
	mFogFlareDegree.isDefault = 1;
	
    if(pthread_create(&mTidDefogDetection, NULL, defogDetectionRoutine, (void *)this)) {
        PRINTE("pthread_create error");
        exit(1);
    }

}

FrameProcessor::~FrameProcessor()
{
	
	pthread_mutex_lock(&mThreadMutex); 
	mMsgScene.type =  SCENE_MSG_TYPE_DESTROY ;
	pthread_cond_signal(&mCond);
	pthread_mutex_unlock(&mThreadMutex);
			
	pthread_join(mTidDefogDetection, NULL);

	if (NULL != mDefogProcessor) {
	    delete mDefogProcessor ;
	    mDefogProcessor = NULL;	
	}
	
	pthread_mutex_destroy(&mDefogProcessorMutex);
	pthread_mutex_destroy(&mThreadMutex);
	pthread_cond_destroy(&mCond);
	
}

FrameProcessor* FrameProcessor::mFrameProcessor = NULL;

FrameProcessor*
FrameProcessor::
getInstance()
{
	if (mFrameProcessor == NULL) {
		mFrameProcessor = new FrameProcessor();	
	}

	return mFrameProcessor;
}

int32_t
FrameProcessor::
init()
{
	int32_t res = 0;

	return res;
}


void
FrameProcessor::
deinit()
{
	mInitialized = false;
}

int32_t
FrameProcessor::
registerDefogListener(DefogListener* listener)
{
	int32_t res = 0;
	
	mDefogListener = listener;

	return res;
}

void
FrameProcessor::
unregisterDefogListener()
{
	mDefogListener = NULL;
}

int32_t
FrameProcessor::
enableFrameProcess(int32_t maxWidth, int32_t maxHeight, uint32_t format, int32_t featureMask)
{
	int32_t res = 0;

	int32_t mask = GN_CAMERA_FEATURE_DEFOG_SHOT | GN_CAMERA_FEATURE_DEFOG_DETECTION;
	
	if ((featureMask & mask) == 0) {
		return -1;
	}

	mGNFrameProcessMask |= featureMask;

    if (!mInitialized) {
		//init mem
		mMemInitParm = new AlgDefogInitParm;
	    if (mMemInitParm == NULL) {
    		PRINTE("Failed to new mMemInitParm");
			return -1;
	    }
		
	    mMemInitParm->width =  maxWidth;
	    mMemInitParm->height = maxHeight;
	    mMemInitParm->pixelArrayFormat = format;
		
		mDefogProcessor->Init(mMemInitParm);

		pthread_mutex_lock(&mThreadMutex); 	
		mMsgScene.type =  SCENE_MSG_TYPE_CLEAR;
		pthread_cond_signal(&mCond);
		pthread_mutex_unlock(&mThreadMutex);

	}

	mInitialized = true;

	return res;
}

void
FrameProcessor::
disableFrameProcess(int32_t featureMask)
{
	int32_t mask = GN_CAMERA_FEATURE_DEFOG_SHOT | GN_CAMERA_FEATURE_DEFOG_DETECTION;
	
	if ((featureMask & mask) == 0) {
		return;
	}

	mGNFrameProcessMask &= ~featureMask;

	if ((mGNFrameProcessMask & mask) == 0 && mInitialized) {
		if (mDefogProcessor != NULL) {
			mDefogProcessor->Destroy();
		}

		if (mMemInitParm != NULL) {
		    delete mMemInitParm;
		    mMemInitParm = NULL;
		}

		mInitialized = false;
	}
}

int32_t 
FrameProcessor::
getDefogDetectionResult()
{
	int32_t result;

	if (mFogFlareDegree.isDefault == 1) {
		result = 0;//havn't processed for now
	} else {
		result = mFogFlareDegree.result;
	}

	PRINTD("%s result = %d", __func__, result);
	
    if (mDefogListener != NULL) {
		mDefogListener->onCallback(result);
	}

	return result;
}

int32_t
FrameProcessor::
processDefog(PIMGOFFSCREEN srcImg, bool isPreview)
{
	int32_t res = 0;

	if (!mInitialized || srcImg == NULL) {
		PRINTD("[processDefog] failed, mInitialized %d", mInitialized);
		return -1;
	}
	
	if (isPreview) {
		getDefogDetectionResult();
		if (pthread_mutex_trylock(&mThreadMutex) != 0) {
			PRINTE("Failed to trylock mThreadMutex.");
			return -1;
		}
		mMsgScene.srcImg = *srcImg;
		mMsgScene.type = SCENE_MSG_TYPE_PROC;
		pthread_cond_signal(&mCond);
	} else {
		pthread_mutex_lock(&mThreadMutex);
		res = mDefogProcessor->Run(srcImg);
	}
	
	pthread_mutex_unlock(&mThreadMutex);
	
	return res;
}

void *
FrameProcessor::
defogDetectionRoutine(void *data)
{
	int ret;	
	int running = 1;
	IMGOFFSCREEN srcImg;
	FrameProcessor* thiz  = (FrameProcessor*)data;
	
	signed long long end_time;
	struct timeval r;
	struct timespec ts;

    do {		
	  	pthread_mutex_lock(&thiz->mThreadMutex);
    	pthread_cond_wait(&thiz->mCond, &thiz->mThreadMutex);
	  	pthread_mutex_unlock(&thiz->mThreadMutex);
		
		switch (thiz->mMsgScene.type) {			
			case SCENE_MSG_TYPE_PROC:
				pthread_mutex_lock(&thiz->mDefogProcessorMutex);
				thiz->mDefogProcessor->Detection(&(thiz->mMsgScene.srcImg), &thiz->mFogFlareDegree, DETECTION_MODE);
				pthread_mutex_unlock(&thiz->mDefogProcessorMutex);
				break;
			
			case SCENE_MSG_TYPE_DESTROY:
				pthread_exit(0);
				running = 0;
		        break;

			case SCENE_MSG_TYPE_CLEAR:
				break;

			default:
				PRINTE("Invalid msg type");
				break;
		}
    } while (running);

	return NULL;
}
