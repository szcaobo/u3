/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/
 
#define LOG_TAG "GioneeCameraFeature"

#include <android/log.h>
#include <cutils/properties.h>
#include "GioneeCameraFeature.h"

namespace android {
GioneeCameraFeature::GioneeCameraFeature()
    : mGNCameraFeature(GN_CAMERA_FEATURE_NONE)
    , mListener(NULL)
    , mFormat(GN_IMG_FORMAT_NONE)
    , mPreviewBufferSize(0)
    , mWidth(0)
    , mHeight(0)
    , mGioneeDefogShot(NULL)
    , mGioneeDefogDetection(NULL)
{
	mGioneeDefogShot = new GioneeDefogShot();
	if (mGioneeDefogShot == NULL) {
		PRINTD("Failed to new GioneeDefog");
	}

	mGioneeDefogDetection = new GioneeDefogDetection();
	if (mGioneeDefogDetection == NULL) {
		PRINTD("Failed to new GioneeDefogDetection");
	}

    pthread_mutex_init(&mMutex, NULL);
}

GioneeCameraFeature::
~GioneeCameraFeature()
{
    pthread_mutex_destroy(&mMutex);
}

GioneeCameraFeature*
GioneeCameraFeature::
createInstance()
{
    return new GioneeCameraFeature();
}

void
GioneeCameraFeature::
destroyInstance()
{
    delete this;
}

int32
GioneeCameraFeature::
init()
{
    int32 res = 0;

	if (mGioneeDefogShot != NULL) {
		res = mGioneeDefogShot->init();
		if (res != 0) {
			PRINTE("Failed to initialized mGioneeDefogShot [%ld]", res);
		}
	}

	if (mGioneeDefogDetection != NULL) {
		res = mGioneeDefogDetection->init();
		if (res != 0) {
			PRINTE("Failed to initialized mGioneeDefogShot [%ld]", res);
		}
	}
	
    return res;
}

void
GioneeCameraFeature::
deinit()
{
    pthread_mutex_lock(&mMutex);

	if (mGioneeDefogShot!= NULL) {
		mGioneeDefogShot->deinit();
	}

	if (mGioneeDefogDetection!= NULL) {
		mGioneeDefogDetection->deinit();
		mGioneeDefogDetection->setCameraListener(NULL);
	}

    mWidth = 0;
    mHeight = 0;
    mFormat = GN_IMG_FORMAT_NONE;
    mPreviewBufferSize = 0;
    mListener = NULL;

    pthread_mutex_unlock(&mMutex);
}

int32
GioneeCameraFeature::
initPreviewSize(int width, int height, GNImgFormat format)
{
    int32  res = 0;

	pthread_mutex_lock(&mMutex);

	mWidth = width;
	mHeight = height;
	mFormat = format;
	
	mPreviewData.i32Width	= width;
	mPreviewData.i32Height	= height;

	switch (format) {
		case GN_IMG_FORMAT_YV12:
			break;
		case GN_IMG_FORMAT_YUV422:
			break;
		case GN_IMG_FORMAT_NV21:
			mPreviewData.u32PixelArrayFormat 	= IMG_PAF_NV21;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width;
			mPreviewData.pi32Pitch[2] 			= 0;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
		case GN_IMG_FORMAT_YUV420:
			break;
		default:
			mPreviewData.u32PixelArrayFormat 	= IMG_PAF_NV21;
			mPreviewData.pi32Pitch[0] 			= width;
			mPreviewData.pi32Pitch[1] 			= width;
			mPreviewData.pi32Pitch[2] 			= 0;
			mPreviewBufferSize 					= width * height * 3 / 2;
			break;
	}
	
	pthread_mutex_unlock(&mMutex);
	
    return 0;
}

int32
GioneeCameraFeature::
setCameraListener(GNCameraFeatureListener* listener)
{
    pthread_mutex_lock(&mMutex);

    mListener = listener;

	if (mGioneeDefogDetection != NULL) {
		mGioneeDefogDetection->setCameraListener(listener);
	}

    pthread_mutex_unlock(&mMutex);

    return 0;
}

int32
GioneeCameraFeature::
processPreview(void* inputBuffer, int size, int mask)
{
    int32  res = 0;

	pthread_mutex_lock(&mMutex);

	if (inputBuffer == NULL || size == 0 || size < mPreviewBufferSize) {
		PRINTE("the inputBuffer is null or invalid.");
		pthread_mutex_unlock(&mMutex); 
		return -1;
	}

	if ((mGNCameraFeature & 0XFFFF) == 0) {
		pthread_mutex_unlock(&mMutex); 
		return res;
	}
	
	mPreviewData.ppu8Plane[0] = (uint8_t*)inputBuffer;

	switch (mFormat) {
		case GN_IMG_FORMAT_YV12:
			break;
		case GN_IMG_FORMAT_YUV422:
			break;
		case GN_IMG_FORMAT_NV21:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_YUV420:
			break;
		default:
			mPreviewData.ppu8Plane[1] = mPreviewData.ppu8Plane[0] + mPreviewData.i32Width * mPreviewData.i32Height;
			mPreviewData.ppu8Plane[2] = NULL;
			break;
	}
	
	if ((mGNCameraFeature & GN_CAMERA_FEATURE_DEFOG_DETECTION) & mask) {
		res = processDefogDetection(&mPreviewData);
	}

	pthread_mutex_unlock(&mMutex); 
	
    return res;
}

int32
GioneeCameraFeature::
processRaw(void* inputBuffer, int size, int width, int height, GNImgFormat format, int mask)
{
    int32 res = 0;
	int yStride = 0;
    int yScanline = 0;
	IMGOFFSCREEN imageData;

	if (inputBuffer == NULL || size == 0 || width == 0 || height == 0) {
		PRINTE("Invalid input buffer.");
		return -1;
	}

	pthread_mutex_lock(&mMutex);

	imageData.i32Width	= width;
	imageData.i32Height = height;

	if (ALIGN_FORMAT != ALIGN_TO_32) {
		yStride = ALIGN_TO_SIZE(width, ALIGN_FORMAT);
		yScanline = ALIGN_TO_SIZE(height, ALIGN_FORMAT);
	} else {
		yStride = width;
		yScanline = height;
	}

	switch (format) {
		case GN_IMG_FORMAT_YV12:
			imageData.u32PixelArrayFormat = IMG_PAF_YV12;
			imageData.pi32Pitch[0] = yStride;
			imageData.pi32Pitch[1] = yStride / 2;
			imageData.pi32Pitch[2] = yStride / 2;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = imageData.ppu8Plane[0] + yStride * yScanline;
			imageData.ppu8Plane[2] = imageData.ppu8Plane[1] + yStride * yScanline / 4;
			break;
		case GN_IMG_FORMAT_YUYV:
			imageData.u32PixelArrayFormat = IMG_PAF_YUYV;
			imageData.pi32Pitch[0] = yStride * 2;
			imageData.pi32Pitch[1] = 0;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = NULL;
			imageData.ppu8Plane[2] = NULL;
			break;
		case GN_IMG_FORMAT_NV21:
			imageData.u32PixelArrayFormat = IMG_PAF_NV21;
			imageData.pi32Pitch[0] = yStride;
			imageData.pi32Pitch[1] = yStride;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = imageData.ppu8Plane[0] + yStride * yScanline;
			imageData.ppu8Plane[2] = NULL;
			break;
		default:
			imageData.u32PixelArrayFormat = IMG_PAF_YUYV;
			imageData.pi32Pitch[0] = yStride * 2;
			imageData.pi32Pitch[1] = 0;
			imageData.pi32Pitch[2] = 0;
			imageData.ppu8Plane[0] = (uint8_t*)inputBuffer;
			imageData.ppu8Plane[1] = NULL;
			imageData.ppu8Plane[2] = NULL;
			break;
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATURE_DEFOG_SHOT) & mask) {
		res = processDefogShot(&imageData);
		if (res != 0) {
			PRINTD("Failed to process defog");
		}
	}
	
	pthread_mutex_unlock(&mMutex);    

    return res;
}

int32
GioneeCameraFeature::
setDefogShot(GNDefogShot_t defogShotMode,int32 maxWidth,int32 maxHeight,GNImgFormat format)
{
	int32 res = 0;
	
	uint32_t pixelFormat = getPixelFormat(format);
	
	if (defogShotMode == GN_DEFOG_SHOT_ON) {
		if (mGioneeDefogShot != NULL) {
			mGioneeDefogShot->enableDefogShot(maxWidth, maxHeight, pixelFormat);
		}
		
		mGNCameraFeature |= GN_CAMERA_FEATURE_DEFOG_SHOT;
	} else {
		if (mGioneeDefogShot != NULL) {
			mGioneeDefogShot->disableDefogShot();
		}

		mGNCameraFeature &= ~GN_CAMERA_FEATURE_DEFOG_SHOT;
	}

	return res;
}

int32
GioneeCameraFeature::
setDefogDetection(GNDefogDetection_t defogMode, int32 maxWidth,int32 maxHeight,GNImgFormat format)
{
	int32 res = 0;

	uint32_t pixelFormat = getPixelFormat(format);
	
	if (defogMode == GN_DEFOG_DETECTION_ON) {
		if (mGioneeDefogDetection!= NULL) {
			mGioneeDefogDetection->enableDefogDetection(maxWidth, maxHeight, pixelFormat);
		}
		
		mGNCameraFeature |= GN_CAMERA_FEATURE_DEFOG_DETECTION;
	} else {
		if (mGioneeDefogDetection != NULL) {
			mGioneeDefogDetection->disableDefogDetection();
		}

		mGNCameraFeature &= ~GN_CAMERA_FEATURE_DEFOG_DETECTION;
	}

	return res;
}


int32 
GioneeCameraFeature::
processDefogShot(PIMGOFFSCREEN imgSrc)
{
	int32 res = 0;

	if (mGioneeDefogShot != NULL) {
		res = mGioneeDefogShot->processDefogShot(imgSrc);
	}

	return res;
}

int32 
GioneeCameraFeature::
processDefogDetection(PIMGOFFSCREEN imgSrc)
{
	int32 res = 0;
	
	if (mGioneeDefogDetection != NULL) {
		res = mGioneeDefogDetection->processDefogDetection(imgSrc);
	}

	return res;
}

uint32_t
GioneeCameraFeature::
getPixelFormat(GNImgFormat format)
{
	uint32_t pixelFormat = 0;

	switch(format) {
		case GN_IMG_FORMAT_NV21:
			pixelFormat = IMG_PAF_NV21;
			break;
		case GN_IMG_FORMAT_YV12:
			pixelFormat = IMG_PAF_YV12;
			break;
		case GN_IMG_FORMAT_YUYV:
			pixelFormat = IMG_PAF_YUYV;
			break;
		default:
			break;
	}

	return pixelFormat;
}

};
