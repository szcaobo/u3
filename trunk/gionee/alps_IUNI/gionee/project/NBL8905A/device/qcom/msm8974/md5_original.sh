#!/system/bin/sh

touch /data/aurora/systemfilelist
touch /data/aurora/md5_original.file
chmod 777 /data/aurora/systemfilelist
chmod 777 /data/aurora/md5_original.file

cat /dev/null > /data/aurora/systemfilelist
cat /dev/null >  /data/aurora/md5_original.file
sync
isDir(){
    local dirName=$1
    if [ ! -d $dirName ]; then
    return 1
    else
    return 0
    fi
}

recursionDir(){
    local dir=$1
    local filelist=`ls  "${dir}"`
    for filename in $filelist
    do
        local fullpath="$dir"/"${filename}";
        if isDir "${fullpath}";then
            recursionDir "${fullpath}"
        else
            echo "${fullpath}"  >> /data/aurora/systemfilelist
        fi
    done
}

echo "recurse original file."
recursionDir /system/app
recursionDir /system/priv-app
recursionDir /system/bin
recursionDir /system/xbin
recursionDir /system/framework


echo "create original md5."
for file in `cat /data/aurora/systemfilelist`
do
#   [ -z "${file}" -o "${file:0:1}" == "#" ] && continue
   [ -z "${file}" -o "${file:0:1}" == "#" -o ${file:(-6):6} == "run-as" ] && continue
   tmp=`md5 ${file}`
   md5value=${tmp:0:32}
   echo "${file}:${md5value}"  >> /data/aurora/md5_original.file
done
rm /data/aurora/systemfilelist
sync
