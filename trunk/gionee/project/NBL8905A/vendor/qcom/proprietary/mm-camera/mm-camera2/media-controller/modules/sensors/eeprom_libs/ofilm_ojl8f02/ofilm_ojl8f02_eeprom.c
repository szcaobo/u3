/*============================================================================

  Copyright (c) 2013 Qualcomm Technologies, Inc. All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

============================================================================*/
#include <stdio.h>
#include <dlfcn.h>
#include <math.h>
#include <stdlib.h>
#include "chromatix.h"
#include "eeprom.h"
#include "sensor_common.h"

//#define OFILM_OJL8F02_DEBUG

#undef DEBUG_INFO
#ifdef OFILM_OJL8F02_DEBUG
#define DEBUG_INFO(fmt, args...) SERR(fmt, ##args)
#else
#define DEBUG_INFO(fmt, args...) do { } while (0)
#endif


#define BASE_ADDR 0x7010
#define LENS_FLAG_ADDR 0x7028
#define VCM_FLAG_ADDR 0x7021

#define WB_OFFSET (WB_FLAG_ADDR-BASE_ADDR)
#define WB_GROUP_SIZE 5

#define LENS_OFFSET (LENS_FLAG_ADDR-BASE_ADDR)
#define VCM_OFFSET (VCM_FLAG_ADDR-BASE_ADDR)

#define AWB_REG_SIZE 6
#define LSC_REG_SIZE 240

#define REG_GAIN_BASE 0x400

#define RG_RATIO_TYPICAL_VALUE 0x137
#define BG_RATIO_TYPICAL_VALUE 0x161
#define ABS(x)            (((x) < 0) ? -(x) : (x))

struct otp_struct {
  uint16_t module_integrator_id;
  uint16_t lens_id;
  uint16_t production_year;
  uint16_t production_month;
  uint16_t production_day;
  uint16_t rg_ratio;
  uint16_t bg_ratio;
  uint16_t light_rg;
  uint16_t light_bg;
  uint16_t user_data[5];
  uint16_t lenc[62];
  uint16_t VCM_start ;
  uint16_t VCM_end ;
  uint16_t VCM_dir ;
} otp_data;

struct msm_camera_i2c_reg_array g_reg_array[AWB_REG_SIZE+LSC_REG_SIZE];
struct msm_camera_i2c_reg_setting g_reg_setting;

/** ofilm_ojl8f02_get_calibration_items:
 *    @e_ctrl: point to sensor_eeprom_data_t of the eeprom device
 *
 * Get calibration capabilities and mode items.
 *
 * This function executes in eeprom module context
 *
 * Return: void.
 **/
void ofilm_ojl8f02_get_calibration_items(void *e_ctrl)
{
  sensor_eeprom_data_t *ectrl = (sensor_eeprom_data_t *)e_ctrl;
  eeprom_calib_items_t *e_items = &(ectrl->eeprom_data.items);
  e_items->is_insensor = TRUE;
  e_items->is_afc = FALSE;
  e_items->is_wbc = TRUE;
  e_items->is_lsc = TRUE;
  e_items->is_dpc = FALSE;

}

/** ofilm_ojl8f02_calc_otp:
 *
 * Calculate R/B target value for otp white balance data format
 *
 * This function executes in eeprom module context
 *
 * Return: void.
 */
static void ofilm_ojl8f02_calc_otp(uint16_t r_ratio, uint16_t b_ratio,
  uint16_t *r_target, uint16_t *b_target, uint16_t r_offset, uint16_t b_offset)
{
  if ((b_offset * ABS(RG_RATIO_TYPICAL_VALUE - r_ratio))
    < (r_offset * ABS(BG_RATIO_TYPICAL_VALUE - b_ratio))) {
    if (b_ratio < BG_RATIO_TYPICAL_VALUE)
      *b_target = BG_RATIO_TYPICAL_VALUE - b_offset;
    else
      *b_target = BG_RATIO_TYPICAL_VALUE + b_offset;

    if (r_ratio < RG_RATIO_TYPICAL_VALUE) {
      *r_target = RG_RATIO_TYPICAL_VALUE
        - ABS(BG_RATIO_TYPICAL_VALUE - *b_target)
        * ABS(RG_RATIO_TYPICAL_VALUE - r_ratio)
        / ABS(BG_RATIO_TYPICAL_VALUE - b_ratio);
    } else {
      *r_target = RG_RATIO_TYPICAL_VALUE
        + ABS(BG_RATIO_TYPICAL_VALUE - *b_target)
        * ABS(RG_RATIO_TYPICAL_VALUE - r_ratio)
        / ABS(BG_RATIO_TYPICAL_VALUE - b_ratio);
    }
  } else {
    if (r_ratio < RG_RATIO_TYPICAL_VALUE)
      *r_target = RG_RATIO_TYPICAL_VALUE - r_offset;
    else
      *r_target = RG_RATIO_TYPICAL_VALUE + r_offset;

    if (b_ratio < BG_RATIO_TYPICAL_VALUE) {
      *b_target = BG_RATIO_TYPICAL_VALUE
        - ABS(RG_RATIO_TYPICAL_VALUE - *r_target)
        * ABS(BG_RATIO_TYPICAL_VALUE - b_ratio)
        / ABS(RG_RATIO_TYPICAL_VALUE - r_ratio);
    } else {
      *b_target = BG_RATIO_TYPICAL_VALUE
        + ABS(RG_RATIO_TYPICAL_VALUE - *r_target)
        * ABS(BG_RATIO_TYPICAL_VALUE - b_ratio)
        / ABS(RG_RATIO_TYPICAL_VALUE - r_ratio);
    }
  }
}



/** ofilm_ojl8f02_calc_gain:
 *
 * Calculate white balance gain
 *
 * This function executes in eeprom module context
 *
 * Return: void.
 **/
static void ofilm_ojl8f02_calc_gain(uint16_t R_target,
  uint16_t B_target, uint16_t *R_gain, uint16_t *B_gain, uint16_t *G_gain)
{
  if (otp_data.bg_ratio < B_target) {
    if (otp_data.rg_ratio < R_target) {
      *G_gain = REG_GAIN_BASE;
      *B_gain = REG_GAIN_BASE *
        B_target /
        otp_data.bg_ratio;
      *R_gain = REG_GAIN_BASE *
        R_target /
        otp_data.rg_ratio;
    } else {
      *R_gain = REG_GAIN_BASE;
      *G_gain = REG_GAIN_BASE *
        otp_data.rg_ratio /
        R_target;
      *B_gain = REG_GAIN_BASE * otp_data.rg_ratio * B_target
        / (otp_data.bg_ratio * R_target);
    }
  } else {
    if (otp_data.rg_ratio < R_target) {
      *B_gain = REG_GAIN_BASE;
      *G_gain = REG_GAIN_BASE *
        otp_data.bg_ratio /
        B_target;
      *R_gain = REG_GAIN_BASE * otp_data.bg_ratio * R_target
        / (otp_data.rg_ratio * B_target);
    } else {
      if (B_target * otp_data.rg_ratio < R_target * otp_data.bg_ratio) {
        *B_gain = REG_GAIN_BASE;
        *G_gain = REG_GAIN_BASE *
          otp_data.bg_ratio /
          B_target;
        *R_gain = REG_GAIN_BASE * otp_data.bg_ratio * R_target
        / (otp_data.rg_ratio * B_target);
      } else {
        *R_gain = REG_GAIN_BASE;
        *G_gain = REG_GAIN_BASE *
          otp_data.rg_ratio /
          R_target;
        *B_gain = REG_GAIN_BASE * otp_data.rg_ratio * B_target
        / (otp_data.bg_ratio * R_target);
      }
    }
  }
}


/** ofilm_ojl8f02_update_awb:
 *
 * Calculate and apply white balance calibration data
 *
 * This function executes in eeprom module context
 *
 * Return: void.
 **/
static void ofilm_ojl8f02_update_awb()
{
	uint16_t R_gain, G_gain, B_gain;
	uint16_t G_gain_R, G_gain_B ;
	uint16_t nR_G_gain, nB_G_gain, nG_G_gain;
	uint16_t nBase_gain;

	uint16_t R_offset_outside, B_offset_outside;
	uint16_t R_offset_inside, B_offset_inside;
	uint16_t R_target, B_target;

	if(otp_data.light_rg) {
		otp_data.rg_ratio = otp_data.rg_ratio * (otp_data.light_rg +512) / 1024;
	}
	if(otp_data.light_bg){
		otp_data.bg_ratio = otp_data.bg_ratio * (otp_data.light_bg +512) / 1024;
	}
	DEBUG_INFO("rg_ratio=0x%x,bg_ratio=0x%x,light_rg=0x%x,light_bg=0x%x",
		otp_data.rg_ratio,otp_data.bg_ratio,otp_data.light_rg,otp_data.light_bg) ;

	#if 1
	nR_G_gain = (RG_RATIO_TYPICAL_VALUE*1000) / otp_data.rg_ratio;
	nB_G_gain = (BG_RATIO_TYPICAL_VALUE*1000) / otp_data.bg_ratio;
	nG_G_gain = 1000;
	if (nR_G_gain < 1000 || nB_G_gain < 1000)
	{
		if (nR_G_gain < nB_G_gain)
		  nBase_gain = nR_G_gain;
		else
		  nBase_gain = nB_G_gain;
	} else {
		nBase_gain = nG_G_gain;
	}
	R_gain = REG_GAIN_BASE * nR_G_gain / (nBase_gain);
	B_gain = REG_GAIN_BASE * nB_G_gain / (nBase_gain);
	G_gain = REG_GAIN_BASE * nG_G_gain / (nBase_gain);
	
	#else
	
	R_offset_inside = RG_RATIO_TYPICAL_VALUE / 100;
	R_offset_outside = RG_RATIO_TYPICAL_VALUE * 3 / 100;
	B_offset_inside = BG_RATIO_TYPICAL_VALUE / 100;
	B_offset_outside = BG_RATIO_TYPICAL_VALUE * 3 / 100;

	if ((ABS(otp_data.rg_ratio - RG_RATIO_TYPICAL_VALUE)
		< R_offset_inside)
		&& (ABS(otp_data.bg_ratio - BG_RATIO_TYPICAL_VALUE)
		< B_offset_inside)) {
		R_gain = REG_GAIN_BASE;
		G_gain = REG_GAIN_BASE;
		B_gain = REG_GAIN_BASE;
		SLOW("both rg and bg offset be within 1%%");
	} else {
		if ((ABS(otp_data.rg_ratio - RG_RATIO_TYPICAL_VALUE)
			  < R_offset_outside)
			  && (ABS(otp_data.bg_ratio - BG_RATIO_TYPICAL_VALUE)
			  < B_offset_outside)) {
			  ofilm_ojl8f02_calc_otp(otp_data.rg_ratio, otp_data.bg_ratio
				, &R_target, &B_target,
				R_offset_inside, B_offset_inside);
			  SLOW("both rg and bg offset be within 1%% to 3%%");
		} else {
			  ofilm_ojl8f02_calc_otp(otp_data.rg_ratio, otp_data.bg_ratio
				, &R_target, &B_target,
				R_offset_outside, B_offset_outside);
		}
		ofilm_ojl8f02_calc_gain(R_target,B_target,&R_gain,&B_gain,&G_gain);
	}
	#endif

	DEBUG_INFO("R_gain=0x%x,G_gain=0x%x,B_gain=0x%x",R_gain,G_gain,B_gain) ;

	if (R_gain >= REG_GAIN_BASE) {
		g_reg_array[g_reg_setting.size].reg_addr = 0x5032;
		g_reg_array[g_reg_setting.size].reg_data = R_gain >> 8;
		g_reg_setting.size++;
		g_reg_array[g_reg_setting.size].reg_addr = 0x5033;
		g_reg_array[g_reg_setting.size].reg_data = R_gain & 0x00ff;
		g_reg_setting.size++;
	}
	if (G_gain >= REG_GAIN_BASE) {
		g_reg_array[g_reg_setting.size].reg_addr = 0x5034;
		g_reg_array[g_reg_setting.size].reg_data = G_gain >> 8;
		g_reg_setting.size++;
		g_reg_array[g_reg_setting.size].reg_addr = 0x5035;
		g_reg_array[g_reg_setting.size].reg_data = G_gain & 0x00ff;
		g_reg_setting.size++;
	}
	if (B_gain >= REG_GAIN_BASE) {
		g_reg_array[g_reg_setting.size].reg_addr = 0x5036;
		g_reg_array[g_reg_setting.size].reg_data = B_gain >> 8;
		g_reg_setting.size++;
		g_reg_array[g_reg_setting.size].reg_addr = 0x5037;
		g_reg_array[g_reg_setting.size].reg_data = B_gain & 0x00ff;
		g_reg_setting.size++;
	}
}

/** ofilm_ojl8f02_get_group_index:
 *    @mid: group index register value
 *
 * Get which group is used
 *
 * This function executes in eeprom module context
 *
 * Return: int to indicate group index.
 **/
static int ofilm_ojl8f02_get_group_index(uint8_t mid)
{
	int8_t group_index = -1 ;
	
	mid = mid & 0xFC ;
	if((mid&0xC0) == 0x40){
		group_index = 0 ;
	}else if((mid&0x30) == 0x10){
		group_index = 1 ;
	}else if((mid&0x0C) == 0x04){
		group_index = 2 ;
	}else{
		group_index = -1 ;
	}
	return group_index ;
}

/** ofilm_ojl8f02_read_info:
 *    @e_ctrl: point to sensor_eeprom_data_t of the eeprom device
 *
 * Read the data structure of product information like product date
 *
 * This function executes in eeprom module context
 *
 * Return: int to indicate read information success or not.
 **/
static int ofilm_ojl8f02_read_info(sensor_eeprom_data_t *e_ctrl)
{
	uint8_t mid,temp,reg_val;
//Gionee <zhaocuiqin> <2015-01-21> modify for CR01440095 begin
#ifdef ORIGINAL_VERSION
       int i = 0, group_index,addr_offset = 0,group_offset=5;
#else
	int i = 0, group_index,addr_offset = 0,group_offset=8;
#endif
//Gionee <zhaocuiqin> <2015-01-21> modify for CR01440095 end

	SLOW("Enter");
	mid = (uint8_t)(e_ctrl->eeprom_params.buffer[0]);
	if((group_index=ofilm_ojl8f02_get_group_index(mid))==-1){
		SERR("%s:invalid or empty opt data",__func__) ;
		return -1 ;
	}
	addr_offset = group_offset*group_index ;

	otp_data.module_integrator_id =
		(uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 1]);
	otp_data.lens_id =
		(uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 2]);
	otp_data.production_year =
		(uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 3]);
	otp_data.production_month =
		(uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 4]);
	otp_data.production_day =
		(uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 5]);

	temp = (uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 8]);
	DEBUG_INFO("temp=0x%x",temp) ;
	
	reg_val = (uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 6]);
	DEBUG_INFO("reg_val=0x%x",reg_val) ;
	otp_data.rg_ratio = (reg_val<<2) + ((temp>>6)&0x03) ;

	reg_val = (uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + 7]);
	DEBUG_INFO("reg_val=0x%x",reg_val) ;
	otp_data.bg_ratio = (reg_val<<2) + ((temp>>4)&0x03) ;

	return 0;
}

/** ofilm_ojl8f02_format_wbdata:
 *    @e_ctrl: point to sensor_eeprom_data_t of the eeprom device
 *
 * Format the data structure of white balance calibration
 *
 * This function executes in eeprom module context
 *
 * Return: void.
 **/
static void ofilm_ojl8f02_format_wbdata(sensor_eeprom_data_t *e_ctrl)
{
  SLOW("Enter");
  int rc = 0;

  rc = ofilm_ojl8f02_read_info(e_ctrl) ;  
  if(rc < 0) {
    SERR("read wbdata failed");
	return;
  }
  ofilm_ojl8f02_update_awb();

  SLOW("Exit");
}


/** ofilm_ojl8f02_format_lensshading:
 *    @e_ctrl: point to sensor_eeprom_data_t of the eeprom device
 *
 * Format the data structure of lens shading correction calibration
 *
 * This function executes in eeprom module context
 *
 * Return: void.
 **/
void ofilm_ojl8f02_format_lensshading(sensor_eeprom_data_t *e_ctrl)
{
	int addr_offset = -1 ;
	int group_index  = -1 ;
	int group_offset = LSC_REG_SIZE + 1 ;
	int j = 0 ;
	uint8_t mid;

	mid = (uint8_t)(e_ctrl->eeprom_params.buffer[LENS_OFFSET]);

	if((group_index=ofilm_ojl8f02_get_group_index(mid))==-1){
		SERR("%s:invalid or empty lensshading data",__func__) ;
		return ;
	}

	addr_offset = LENS_OFFSET + group_index*group_offset ;
	for (j = 0; j < LSC_REG_SIZE; j++) {
		g_reg_array[g_reg_setting.size].reg_addr = 0x5800 + j;
		g_reg_array[g_reg_setting.size].reg_data =
		  (uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + j + 1]);
		g_reg_setting.size++;
		DEBUG_INFO("reg_val=0x%x",
		  (uint8_t)(e_ctrl->eeprom_params.buffer[addr_offset + j + 1])) ;
	}
	SLOW("Exit");
}

/** ofilm_ojl8f02_format_calibration_data:
 *    @e_ctrl: point to sensor_eeprom_data_t of the eeprom device
 *
 * Format all the data structure of calibration
 *
 * This function executes in eeprom module context and generate
 *   all the calibration registers setting of the sensor.
 *
 * Return: void.
 **/
void ofilm_ojl8f02_format_calibration_data(void *e_ctrl) {
	SLOW("Enter");
	sensor_eeprom_data_t *ectrl = (sensor_eeprom_data_t *)e_ctrl;
	uint8_t *data = ectrl->eeprom_params.buffer;

	g_reg_setting.addr_type = MSM_CAMERA_I2C_WORD_ADDR;
	g_reg_setting.data_type = MSM_CAMERA_I2C_BYTE_DATA;
	g_reg_setting.reg_setting = &g_reg_array[0];
	g_reg_setting.size = 0;
	g_reg_setting.delay = 0;
	ofilm_ojl8f02_format_wbdata(ectrl);
	ofilm_ojl8f02_format_lensshading(ectrl);

	/* zero array size will cause camera crash, tanrifei, 20140102*/
	if (g_reg_setting.size ==0) {
	  g_reg_array[g_reg_setting.size].reg_addr = 0x5032;
	  g_reg_array[g_reg_setting.size].reg_data = (REG_GAIN_BASE>>8) & 0xFF;
	  g_reg_setting.size++;
	  g_reg_array[g_reg_setting.size].reg_addr = 0x5033;
	  g_reg_array[g_reg_setting.size].reg_data = (REG_GAIN_BASE & 0xFF);
	  g_reg_setting.size++;
	}
	/* add end */  

	SLOW("Exit");
}

/** ofilm_ojl8f02_get_raw_data:
 *    @e_ctrl: point to sensor_eeprom_data_t of the eeprom device
 *    @data: point to the destination msm_camera_i2c_reg_setting
 *
 * Get the all the calibration registers setting of the sensor
 *
 * This function executes in eeprom module context.
 *
 * Return: void.
 **/
int32_t ofilm_ojl8f02_get_raw_data(void *e_ctrl, void *data) {
  if (e_ctrl && data)
    memcpy(data, &g_reg_setting, sizeof(g_reg_setting));
  else
    SERR("failed Null pointer");
  return 0;
}

static eeprom_lib_func_t ofilm_ojl8f02_lib_func_ptr = {
  .get_calibration_items = ofilm_ojl8f02_get_calibration_items,
  .format_calibration_data = ofilm_ojl8f02_format_calibration_data,
  .do_af_calibration = NULL,
  .do_wbc_calibration = NULL,
  .do_lsc_calibration = NULL,
  .do_dpc_calibration = NULL,
  .get_dpc_calibration_info = NULL,
  .get_raw_data = ofilm_ojl8f02_get_raw_data,
};

/** ofilm_ojl8f02_eeprom_open_lib:
 *
 * Get the funtion pointer of this lib.
 *
 * This function executes in eeprom module context.
 *
 * Return: eeprom_lib_func_t point to the function pointer.
 **/
void* ofilm_ojl8f02_eeprom_open_lib(void) {
  return &ofilm_ojl8f02_lib_func_ptr;
}

