ifeq ("$(APK_WHATSAPPDOWNLOADER_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := WhatsAppDownloader
LOCAL_SRC_FILES := WhatsAppDownloader.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/app/$(LOCAL_SRC_FILES) 
endif

##install rom and can uninstall by mtk method
#apk path: system/vendor/operator/app/
#lib path: system/vendor/lib/
ifeq ("$(APK_WHATSAPPDOWNLOADER_SUPPORT)","yes_mtk")
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := WhatsAppDownloader
LOCAL_SRC_FILES := WhatsAppDownloader.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/vendor/operator/app/$(LOCAL_SRC_FILES) 
endif
##GIONEE heling 20140624 modify  begin
ifeq ("$(APK_WHATSAPPDOWNLOADER_SUPPORT)","yes_data")
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := WhatsAppDownloader
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):data/app/$(LOCAL_SRC_FILES) \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):system/gn_app_bk/$(LOCAL_SRC_FILES)

endif
##GIONEE heling 20140624 modify end

#Gionee lijinfang 2015-3-22 modify for CR01456635 begin
ifeq ("$(APK_WHATSAPPDOWNLOADER_SUPPORT)","yes_tcard")
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := WhatsAppDownloader
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/$(LOCAL_SRC_FILES):tcard/system/gn_app_bk/$(LOCAL_MODULE)/$(LOCAL_SRC_FILES)

endif
#Gionee lijinfang 2015-3-22 modify for CR01456635 end
