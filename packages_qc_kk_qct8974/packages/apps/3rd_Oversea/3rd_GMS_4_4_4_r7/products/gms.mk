#
# Copyright (C) 2008 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# GMS Mandatory Apps and libraries
ifeq ("$(GN3RD_GMS_SUPPORT)","4_4_4_r7")
PRODUCT_PACKAGES += \
    ConfigUpdater \
    GoogleBackupTransport \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    GoogleFeedback \
    GoogleLoginService \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    com.google.android.media.effects.jar \
    google_generic_update.txt



ifneq ($(findstring SetupWizard,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += SetupWizard
endif

# Chrome Browser: Chrome is the only preloaded system browser
# Use "ChromeWithBrowser" if Chrome is preloaded along with another browser side-by-sde
#GIONEE futao 20140919 modify for CR01387719 begin
#GIONEE: chenle 2014-10-09 modify CR01394099 begin for GMS chrome
ifneq ($(findstring OnlyChrome,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Chrome
else
ifneq ($(findstring Chrome,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += ChromeWithBrowser
endif
endif
#GIONEE: chenle 2014-10-09 modify CR01394099 end for GMS chrome
#GIONEE futao 20140919 modify for CR01387719 end
#PRODUCT_PACKAGES += ChromeWithBrowser

# Uncomment this if you want to integrate customized homepage provider for Chrome
# For the details, see Android.mk in apps directory
#PRODUCT_PACKAGES += ChromeCustomizations

# GMS Mandatory Apps (unbundled)
#PRODUCT_PACKAGES += \
#    Books \
#    Drive \
#    Gmail2 \
#    GmsCore \
#    Hangouts \
#    Magazines \
#    Maps \
#    Music2 \
#    PlayGames \
#    PlusOne \
#    Street \
#    Velvet \
#    Videos \
#    YouTube

# GMS Optional Apps
#PRODUCT_PACKAGES += \
#    CalendarGoogle \
#    CloudPrint \
#    DeskClockGoogle \
#    FaceLock \
#    GalleryGoogle \
#    GenieWidget \
#    GoogleCamera \
#    GoogleHome \
#    GoogleTTS \
#    Keep \
#    LatinImeGoogle \
#    TagGoogle \
#    talkback

#######################################
PRODUCT_PACKAGES += \
    GmsCore \
    Velvet

ifneq ($(findstring Books,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Books
endif

ifneq ($(findstring Drive,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Drive
endif

ifneq ($(findstring Gmail,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Gmail2
endif

ifneq ($(findstring Hangouts,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Hangouts
endif

ifneq ($(findstring Magazines,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Magazines
endif

ifneq ($(findstring Maps,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Maps
endif

ifneq ($(findstring Music2,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Music2
endif

ifneq ($(findstring PlayGames,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += PlayGames
endif

ifneq ($(findstring PlusOne,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += PlusOne
endif

ifneq ($(findstring Street,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Street
endif

ifneq ($(findstring Videos,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Videos
endif

ifneq ($(findstring YouTube,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += YouTube
endif

##################################
ifneq ($(findstring CalendarGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += CalendarGoogle
endif

ifneq ($(findstring CloudPrint,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += CloudPrint
endif

ifneq ($(findstring DeskClockGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += DeskClockGoogle
endif

ifneq ($(findstring FaceLock,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += FaceLock
endif

ifneq ($(findstring GalleryGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GalleryGoogle
endif

ifneq ($(findstring GenieWidget,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GenieWidget
endif

ifneq ($(findstring VideoEditorGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleCamera
endif

ifneq ($(findstring GoogleHome,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleHome
endif

ifneq ($(findstring GoogleTTS,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleTTS
endif

ifneq ($(findstring Keep,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Keep
endif

ifneq ($(findstring LatinImeGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += LatinImeGoogle
endif

ifneq ($(findstring TagGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += TagGoogle
endif

#GIONEE: chenle 2014-10-11 modify CR01394639 begin for GMS
ifneq ($(findstring Talkback,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += talkback
endif
#GIONEE: chenle 2014-10-11 modify CR01394639 end for GMS

# MediaUploader is deprecated.
# Do not uncomment this if this GMS pack is used for newly launching device.
#PRODUCT_PACKAGES += MediaUploader

# Disable building webviewchromium from source
#ifeq ("$(GN_PRODUCT_PLATFORM)","QCOM")
PRODUCT_PREBUILT_WEBVIEWCHROMIUM := yes
#else
#ifeq ("$(GN_BUILD_GOOGLE_ORG_CHROMIUM)","yes")
#PRODUCT_PREBUILT_WEBVIEWCHROMIUM := yes
#else
#PRODUCT_PREBUILT_WEBVIEWCHROMIUM := no
#endif
#endif

# Overlay for Google network and fused location providers
$(call inherit-product, device/sample/products/location_overlay.mk)

# Overrides
PRODUCT_PROPERTY_OVERRIDES += \
    ro.setupwizard.mode=OPTIONAL \
    ro.com.google.gmsversion=4.4_r7

PRODUCT_PROPERTY_OVERRIDES += \
      ro.com.google.clientidbase=android-gionee \
      ro.com.google.clientidbase.ms=android-gionee \
      ro.com.google.clientidbase.yt=android-gionee \
      ro.com.google.clientidbase.am=android-gionee \
      ro.com.google.clientidbase.gmm=android-gionee
endif
